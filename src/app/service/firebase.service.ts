import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

    constructor(public afs: AngularFirestore) { }

    checkPeserta(value, x){
        var docRef = this.afs.collection('peserta').doc(value);
        
        return docRef.get().toPromise().then((doc) => {
            if (doc.exists) {
                // console.log("Document data:", doc.data());
                if(x == "AM"){
                    docRef.update({ Pagi: "Hadir" });
                    console.log("data: ", doc.data());
                }
                else if(x == "PM")
                    docRef.update({ Petang: "Hadir" });
            } 
            else {
                alert("User not exist");
            }
        });
    }
  
}
