import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { FirebaseService } from '../service/firebase.service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    scannedCode = null;
    parsed = null;

    qrInfo = {
        Nama: '',
        IC: '',
        Emel: '',
        Jabatan: '',
        Daerah: '',
        Pagi: '',
        Petang: ''
    }
   
    constructor(private barcodeScanner: BarcodeScanner, private firebase: FirebaseService) { 
    }
   
    scanCode() {
        this.barcodeScanner.scan().then(barcodeData => {
            this.scannedCode = barcodeData.text;
            // parse string in order to get/store each value to backend
            this.qrInfo = JSON.parse(this.scannedCode);

        }, (err) => {
            console.log('Error: ', err);
        });
    }

    pagi(){
        this.firebase.checkPeserta(this.qrInfo.IC, "AM");
    }

    petang(){
        this.firebase.checkPeserta(this.qrInfo.IC, "PM");
    }

}
